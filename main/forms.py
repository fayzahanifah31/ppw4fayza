from django.forms import ModelForm
from .models import Matkul
from django import forms
class Input_Form(forms.ModelForm):
	class Meta:
		model = Matkul
		fields = ['namaMatkul','dosenPengajar','jumlahSks','deskripsiMatkul','semester','ruangKelas']
	error_messages = {
		'required' : 'Please Type'
	}

	namaMatkul = forms.CharField(label='', required=False, 
    max_length=27)

	jumlahSks = forms.CharField(label='', required=False, 
    max_length=27)

	deskripsiMatkul = forms.CharField(label='', required=False, 
    max_length=27)

	semester = forms.CharField(label='', required=False, 
    max_length=27)

	ruangKelas = forms.CharField(label='', required=False, 
    max_length=27)
