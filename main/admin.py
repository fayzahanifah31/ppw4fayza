from django.contrib import admin

# Register your models here.

from .models import Matkul

class FormAdmin(admin.ModelAdmin):
    class Meta:
        model = Matkul

admin.site.register(Matkul, FormAdmin)
