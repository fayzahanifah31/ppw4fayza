from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('personal/', views.personal, name='personal'),
    path('educational/', views.educational, name='educational'),
    path('Organizational/', views.organizational, name='Organizational'),
    path('thankyou/', views.thankyou, name='thankyou'),

    path('form/', views.form,name = "form"),
    path('form/savename', views.form,name = "form"),
]


