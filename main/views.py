from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Input_Form

def home(request):
    return render(request, 'main/home.html')
def personal(request):
    return render(request, 'main/personal.html')
def educational(request):
    return render(request, 'main/educational.html')
def organizational(request):
    return render(request, 'main/Organizational.html')
def thankyou(request):
    return render(request, 'main/thankyou.html')
def form(request):
    context={
        'input_form' : Input_Form,
    }
    return render(request, 'main/form.html',context)

def savename(request):
    form = Input_Form(request.POST or None)
    if (form.is_valid and request.method == "POST"):
        form.save()
    return render(request, 'form.html', {'form': form})
