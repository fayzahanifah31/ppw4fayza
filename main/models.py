from django.db import models

# Create your models here.
class Matkul(models.Model):
    namaMatkul = models.CharField(max_length=200)
    dosenPengajar= models.CharField(max_length=200)
    jumlahSks= models.CharField(max_length=200)
    deskripsiMatkul=models.CharField(max_length=200)
    semester = models.CharField(max_length=200)
    ruangKelas = models.CharField(max_length=200)
    
    # def get_absolute_url(self):
    #     return f"/main/{self.id}"


class Post(models.Model):
    matkul= models.ForeignKey(Matkul, on_delete = models.CASCADE)
    content = models.CharField(max_length=200)


